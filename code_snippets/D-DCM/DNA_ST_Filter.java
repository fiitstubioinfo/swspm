package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.data.AbstractNumberVector;
import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.SparseFloatVector;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalMultipleID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.BSI_DNA_Numerifier;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_Numerifier;
import de.lmu.ifi.dbs.elki.math.stuba.transform.FFTTransformer;
import de.lmu.ifi.dbs.elki.math.stuba.transform.SpectralTransformer;
import de.lmu.ifi.dbs.elki.math.stuba.window.RectangularWindower;
import de.lmu.ifi.dbs.elki.math.stuba.window.TransformVectorWindower;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.LessEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.DoubleParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/**
 * 
 * Transform filter that creates sparse vector of significant freqs. Use
 * together with SparseVectorComaprator
 * 
 * @author Tomas Farkas
 *
 */
public class DNA_ST_Filter extends AbstractDNAFilter {
  private int maxSeqL;
  private double significantRatio;
  private DNA_Numerifier numerifier;
  private SpectralTransformer transform;
  private TransformVectorWindower windower;

  protected DNA_ST_Filter(DNA_DataProvider provider, DNA_Numerifier numerifier, SpectralTransformer transform, TransformVectorWindower windower, int maxSeqL, double significantRatio) {
    super(provider);
    this.maxSeqL = maxSeqL;
    this.numerifier = numerifier;
    this.transform = transform;
    this.windower = windower;
    this.significantRatio = significantRatio;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));

    if(currentItem instanceof StringVector_externalMultipleID) {
      data[inx] = processTask((StringVector_externalMultipleID) currentItem);
    }
    else {
      data[inx] = processTask(currentItem);
    }
  }

  protected AbstractNumberVector processTask(StringVector_externalID currentItem) {

    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, false, currentItem.getParams());
    if(dataString != null) {
      System.out.println("Processing");
      return getSpectra(dataString);
    }
    else {
      LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams()));
      return new SparseFloatVector(new int[0], new float[0], 0);

    }
  }

  @SuppressWarnings("rawtypes")
  protected MultipleTypeVectorList processTask(StringVector_externalMultipleID currentItem) {

    int fNum = currentItem.getNumberOfFiles();
    List<FeatureVector> result = new ArrayList<>();

    for(int f = 0; f < fNum; f++) {
      String fname = currentItem.getFileName(f);
      char[] dataString = provider.getData(fname, false, currentItem.getParams(f));
      if(dataString != null) {  
        result.add(getSpectra(dataString));
      }
      else {
        LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams(f)));
        result.add(new SparseFloatVector(new int[0], new float[0], 0));
      }
    }
    return new MultipleTypeVectorList(result);
  }

  public AbstractNumberVector getSpectra(char[] dataString) {
    // get numeric representation
    int fftLength = 1 << (int) (Math.log(maxSeqL - 1) / Math.log(2) + 1);
    float[][] numericSeqs = numerifier.numerify(dataString, false);
    //rescale (apply window, apply pad)
    for(int i = 0; i < numericSeqs.length; i++) {
      numericSeqs[i] = windower.applyWindow(numericSeqs[i], numericSeqs[i].length);
      numericSeqs[i] = Arrays.copyOf(numericSeqs[i], fftLength);
    }
    //get spectral transform
    float[] spectrum = transform.getSpectrum(true, true, numericSeqs);

    // normalize
    for(int i = 0; i < spectrum.length; i++) {
      spectrum[i] = spectrum[i] / dataString.length;
    }

    // get most significant values
    int dimensionality = (int) (spectrum.length * significantRatio);

    float[] transformed_sorted = new float[spectrum.length];
    for(int i = 0; i < spectrum.length; i++) {
      transformed_sorted[i] = Math.abs(spectrum[i]);
    }
    Arrays.sort(transformed_sorted);
    float jThQuantile = transformed_sorted[spectrum.length - dimensionality];

    // find number of equal as the quantile
    for(int i = spectrum.length - dimensionality - 1; i >= 0 && transformed_sorted[i] == jThQuantile; i--) {
      dimensionality++;
    }
    float[] values = new float[dimensionality];
    int[] indices = new int[dimensionality];
    int ctr = 0;
    for(int i = 0; i < spectrum.length; i++) {
      if(Math.abs(spectrum[i]) >= jThQuantile) {
        values[ctr] = (spectrum[i]);
        indices[ctr++] = (i);
      }
    }

    return new SparseFloatVector(indices, values, dimensionality);
  }

  @Override
  protected void createArray_hook() {
      data = new SparseFloatVector[getNrOfTasks()];
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    if(data.getClass().getComponentType().equals(SparseFloatVector.class)) {
      res.appendColumn(TypeUtil.SPARSE_VECTOR_FIELD, Arrays.asList(data));
    }
    else {
      res.appendColumn(TypeUtil.MULTIPLE_TYPE_VECTOR, Arrays.asList(data));
    }
    return res;
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID NUMERIFIER = new OptionID("filter.numerifier", "Type of DNA numerification method");
    protected static final OptionID TRANSFORM = new OptionID("filter.transform", "Type of spectral transform");
    protected static final OptionID WINDOW = new OptionID("filter.transformWindow", "Type of window");
    protected static final OptionID SIG_RATIO = new OptionID("filter.significantRatio", "Ratio for significat values, e.g. 0.1 means select top 10%");
    protected static final OptionID MAX_S_LEN = new OptionID("filter.maxSeqLen", "Maximum data length among dataset, bp.");

    private DNA_Numerifier numerifier;
    private SpectralTransformer transform;
    private TransformVectorWindower windower;
    private double sigRatio;
    private int maxSeqL;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      ObjectParameter<DNA_Numerifier> nuP = new ObjectParameter<>(NUMERIFIER, DNA_Numerifier.class, BSI_DNA_Numerifier.class);
      ObjectParameter<SpectralTransformer> trP = new ObjectParameter<>(TRANSFORM, SpectralTransformer.class, FFTTransformer.class);
      ObjectParameter<TransformVectorWindower> winP = new ObjectParameter<>(WINDOW, TransformVectorWindower.class, RectangularWindower.class);

      DoubleParameter sigP = new DoubleParameter(SIG_RATIO);
      sigP.addConstraint(new GreaterConstraint(0));
      sigP.addConstraint(new LessEqualConstraint(1));

      IntParameter slP = new IntParameter(MAX_S_LEN);
      slP.addConstraint(new GreaterConstraint(0));

      if(config.grab(nuP)) {
        numerifier = nuP.instantiateClass(config);
      }
      if(config.grab(trP)) {
        transform = trP.instantiateClass(config);
      }
      if(config.grab(winP)){
        windower = winP.instantiateClass(config);
      }
      if(config.grab(sigP)) {
        sigRatio = sigP.getValue();
      }
      if(config.grab(slP)) {
        maxSeqL = slP.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNA_ST_Filter(provider, numerifier, transform, windower, maxSeqL, sigRatio);
    }

  }
}
