# SWSPM 
Sliding window spectral projection method - is an alignment-free DNA comparison method based on signal processing approaches. This repository contains a software tool with the SWSPM method.
## Repository contents
*  [executables](executables) - contains the executable file and its necessary dependencies
*  [code-snippets](code-snippets) - contains code-snippets of other DNA comparison methods and also an example of correspoding configuration file
*  [data](data) - contains data used in experiments and examples.
*  [full-source](full-source) - contains full source files used for compiling an executable file. See corresponding page for more details.

## Requirements
*  **installed Java JRE and JDK, both of version 8**:  
`sudo apt update && sudo apt-get install openjdk-8-jdk`  
if other versions of Java are already installed, select version 8 by calling:  
`sudo update-alternatives --config java`
*  **installed csh command shell** - if not, proceed with:  
`sudo apt update && sudo apt-get install csh`

## Running the program

### Input config file
Before running the program a input config file needs to be created. This file should be a .csv file with IDs and paths to input DNA sequences. File should contain lines with 3 arguments separated by commas:   
`"ID in output files"`,`"path to an input sequence(s)"`,`"ID of sequences to search for in path"`   
e.g.:  
`Human, ..\data\Mammals.fasta, Human`  
`Gorilla, ..\data\Mammals.fasta, Gorilla`  
`Chimp, ..\data\Chimp.fasta, Chimpanzee`  
Note: ID of sequences in Fasta files must start with '>' symbol. Also sequences of the same species should be concatenated into one sequence.

### Program parameters
An executable file is in the [executables directory](executables). To run a program proceed with the following:  
`./SWPM.sh [Required input] [Additional input]`  
#### Required inputs are:  
	-i <string>     Path to input config file   
	-o <string>     Path where files will be output   
#### Additional input represents auxiliary program parameters:  
	-s <int>        Step of sliding window(default 256)   
	-l <int>        Length of sliding window(default 2048)  
	-c              use for complements-cross method of DNA encoding (default complements together)  

## An example of running a program
At first download this repository and unpack it. Then go to [executables](executables):  
`cd executables`  
For an input data we can use for example `Mammals.fasta` dataset from [data](data). At first we need to create a input config file, for example `Mammals.csv` according to rules stated above.
Then to run a default version of SWSPM:   
`./SWPM.sh -i \path_to_input_config_file_Mammals.csv -o mammals_output`    
To change an additional parameter, e.g. to set a length of a sliding window to 4096 and a size of the window step to 512:  
`./SWPM.sh -i \path_to_input_config_file_Mammals.csv -o mammals_output -l 4096 -s 512`

## Program output
*  resulting tree in Newick format stored as file with suffix _nw.txt
*  resulting matrix of tree traversals stored as file with suffix _tree.txt
*  resulting distance matrix stored as file with suffix _mx.txt

