package de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 * @author Tomas Farkas
 *
 */

public class SubEqualString {
  String value;

  public SubEqualString(String value) {
    this.value = value;
  }

  @Override
  public boolean equals(Object s) {
    if(s.getClass().equals(this.getClass())) {
      //System.out.print(this.value+"-"+((SubEqualString) s).value);
      if(this.value.indexOf(((SubEqualString) s).value) != -1) {
        //System.out.println("="+true);
        return true;
      }
      if(((SubEqualString) s).value.indexOf(this.value) != -1) {
        //System.out.println("="+true);
        return true;
      }

    }
    //System.out.println("="+false);
    return false;
  }

  @Override
  public int hashCode() {
    return 1; // strictly O(n) HashMap but only reasonable solution due to
              // subequality
  }

}
