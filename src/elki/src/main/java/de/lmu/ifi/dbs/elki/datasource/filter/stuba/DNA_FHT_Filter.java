package de.lmu.ifi.dbs.elki.datasource.filter.stuba;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author Tomas Farkas
 *
 */
public class DNA_FHT_Filter /*extends AbstractDNAFilter*/ {

 /* Transform transform;

  int maxLength;

  protected DNA_FHT_Filter(DNA_DataProvider provider, Transform transform, int maxLength) {
    super(provider);
    this.transform = transform;
    this.maxLength = 1 << (int) (Math.log(maxLength - 1) / Math.log(2) + 1);
  }

  protected DNA_FHT_Filter(DNA_DataProvider provider, int maxLength) {
    super(provider);
    this.maxLength = maxLength;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));

    if(currentItem instanceof StringVector_externalMultipleID) {
      data[inx] = processTask((StringVector_externalMultipleID) currentItem);
    }
    else {
      data[inx] = processTask(currentItem);
    }
  }

  public DoubleVector processTask(StringVector_externalID currentItem) {

    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, false, currentItem.getParams());

    if(dataString != null) {
      return new DoubleVector(getSpectra(dataString));
    }
    else {
      LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams()));
      return new DoubleVector(new double[0]);
    }

  }

  @SuppressWarnings("rawtypes")
  public MultipleTypeVectorList processTask(StringVector_externalMultipleID currentItem) {

    int fNum = currentItem.getNumberOfFiles();
    List<FeatureVector> result = new ArrayList<>();

    for(int f = 0; f < fNum; f++) {
      String fname = currentItem.getFileName(f);
      char[] dataString = provider.getData(fname, false, currentItem.getParams(f));

      if(dataString != null) {
        result.add(new DoubleVector(getSpectra(dataString)));
      }
      else {
        LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams(f)));
        result.add(new DoubleVector(new double[0]));
      }
    }
    return new MultipleTypeVectorList(result);
  }

  protected double[] getSpectra(char[] dataString) {
    List<Double> result = new ArrayList<Double>();

    float[] bSI = new float[maxLength];
    for(int i = 0; i < dataString.length; i++) {
      if(dataString[i] == 0) {
        bSI[i] = 1;
      }
    }
    float[] bSItransf = transform.forward(bSI);
    this.addPowerSpectrum(bSItransf, result);

    for(int i = 0; i < dataString.length; i++) {
      if(dataString[i] == 0) {
        bSI[i] = 1;
      }
    }
    bSItransf = transform.forward(bSI);
    this.addPowerSpectrum(bSItransf, result);

    for(int i = 0; i < dataString.length; i++) {
      if(dataString[i] == 0) {
        bSI[i] = 1;
      }
    }
    bSItransf = transform.forward(bSI);
    this.addPowerSpectrum(bSItransf, result);

    for(int i = 0; i < dataString.length; i++) {
      if(dataString[i] == 0) {
        bSI[i] = 1;
      }
    }
    bSItransf = transform.forward(bSI);
    this.addPowerSpectrum(bSItransf, result);

    result.stream().forEach(e -> e = e / dataString.length);
    
    return result.stream().mapToDouble(Double::doubleValue).toArray();
  }

  protected void addPowerSpectrum(float[] data, List<Double> result) {
    int down = data.length / 2;
    int up = data.length;

    while(true) {
      double sum = 0;
      for(int i = down; i < up; i++) {
        sum += data[i];
      }
      result.add(sum);
      up = down;
      down = down / 2;

      if(up == 0) {
        break;
      }
    }
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    if(data.getClass().getComponentType().equals(DoubleVector.class)){
      res.appendColumn(TypeUtil.DOUBLE_VECTOR_FIELD, Arrays.asList(data));
    }else{
      res.appendColumn(TypeUtil.MULTIPLE_TYPE_VECTOR, Arrays.asList(data));
    }
    return res;
  }

  @Override
  protected void createArray_hook() {
    data = new DoubleVector[getNrOfTasks()];
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID WT_TYPE = new OptionID("filter.transformType", "Type of FWT");

    protected Transform transform = null;

    protected static final OptionID WL_TYPE = new OptionID("filter.waveletType", "Typeof wavelet");

    protected Wavelet wavelet = null;

    protected static final OptionID MAX_LEN = new OptionID("filter.maxSeqLen", "Max length of filtered sequence");

    protected int maxLength;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      final ObjectParameter<Wavelet> param1 = new ObjectParameter<>(WL_TYPE, Wavelet.class, Haar1Orthogonal.class);
      if(config.grab(param1)) {
        wavelet = (Wavelet) param1.instantiateClass(config);
      }

      final ObjectParameter<BasicTransform> param2 = new ObjectParameter<>(WT_TYPE, WaveletTransform.class, FastWaveletTransform.class);
      if(config.grab(param2)) {
        WaveletTransform t = (WaveletTransform) param2.instantiateClass(config);
        t.setWavelet(wavelet);
        transform = new Transform(t);
      }

      final IntParameter param3 = new IntParameter(MAX_LEN);
      param3.addConstraint(new GreaterConstraint(0));
      if(config.grab(param3)) {
        maxLength = param3.getValue();
      }
    }

    @Override
    protected DNA_FHT_Filter makeInstance() {
      return new DNA_FHT_Filter(provider, transform, maxLength);
      // return new DNA_FHT_Filter(provider, maxLength);
    }

  }*/
}
