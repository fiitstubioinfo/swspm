package de.lmu.ifi.dbs.elki.result.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.PointerHierarchyRepresentationResult;
import de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.stuba.BinaryTreeResult;
import de.lmu.ifi.dbs.elki.database.Database;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.result.Result;
import de.lmu.ifi.dbs.elki.result.ResultHierarchy;
import de.lmu.ifi.dbs.elki.utilities.datastructures.hierarchy.Hierarchy.Iter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.LabelProvider;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.RelationLabelProvider;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class NewickTreeResultWriter extends MatrixWriter{

  private static final Logging LOG = Logging.getLogger(NewickTreeResultWriter.class);
  
  private LabelProvider labeller;
  
  public NewickTreeResultWriter(LabelProvider labeller, File output) {
    super(output);
    this.labeller = labeller;
  }
  
  @Override
  public void processNewResult(ResultHierarchy hier, Result newResult) {

    if(newResult instanceof Database) {
      labeller.setDatabase((Database) newResult);
    }
    else {
      LOG.warning("Initial database lost");
    }
    
    BinaryTreeResult res = null;
    for(Iter<Result> iter = hier.iterAll(); iter.valid(); iter.advance()) {
      
      if(iter.get().getClass() == BinaryTreeResult.class) {
        res = ((BinaryTreeResult) iter.get());
        break;
      }
      
      if(iter.get().getClass() == PointerHierarchyRepresentationResult.class) {
        res = BinaryTreeResult.fromPHRR((PointerHierarchyRepresentationResult) iter.get());
        break;
      }
    }
    if(res == null) {
      LOG.warning("BinaryTreeResult not found");
      return;
    }    
    
    String newickResult = res.toNewick(labeller);

    FileWriter fw;
    try {
      fw = new FileWriter(output.getAbsoluteFile(), false);
      BufferedWriter bw = new BufferedWriter(fw);
      bw.write(newickResult);
      bw.close();
    }
    catch(IOException e) {
      de.lmu.ifi.dbs.elki.logging.LoggingUtil.exception(e);
    }   
    
    System.out.println(newickResult);
    
  }
  
  public static class Parameterizer extends MatrixWriter.Parameterizer {
   
    public static final OptionID LABEL_PROVIDER_ID = new OptionID("algorithm.labeller", "The class to provide labels for DB objects.");

    LabelProvider labeller;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      final ObjectParameter<LabelProvider> param = new ObjectParameter<>(LABEL_PROVIDER_ID, LabelProvider.class, RelationLabelProvider.class);
      if(config.grab(param)) {
        labeller = (LabelProvider) param.instantiateClass(config);
      }
    }    
    
    protected NewickTreeResultWriter makeInstance() {
      return new NewickTreeResultWriter(labeller, output);
    }
  }


}
