package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;


/*
 * I think you can handle the problem, as stated, for arbitrary alphabet sizes,
 * subject to floating point precision. For an alphabet of n characters, map the
 * text characters to (complex) nth roots of unity. For the pattern characters,
 * map # to 0, and map ordinary characters to the multiplicative inverse of the
 * corresponding text character, and so also nth roots of unity.
 * 
 * You then use the convolution theorem to work out, at each point, the dot
 * product of the text from that point on with the pattern. If the text and the
 * pattern match, each component of the product is either 0 (at a wild card) or
 * 1, from r*r^-1, so if you have a match the result will be m, where m is the
 * number of non-wild card characters in the pattern. If there is not a match,
 * then not all of the components of the dot product will be 1. Thinking of
 * these complex numbers as 2-dimensional vectors, the dot product of these
 * vectors with the vector representing 1 will be less than m, so a mismatch
 * cannot cause a result m and look like a match.
 * 
 * I note that if you divide the text up into buffers of a few times the length
 * of the pattern, you can use an FFT of that length reasonably efficiently, so
 * the complexity is not n log n, where n is the length of the text to be
 * searched, but n log m, where m is the length of the pattern. Despite that, I
 * would need to see benchmark timings before I would believe that this is a
 * competitive method, compared even with a naive search.
 */



/**
 * Distance function based on convolution matching lengths
 * 
 * @author Tomas Farkas
 *
 */
public class DNAFourierCovnolutionFunction extends AbstractDNAFunction {

  protected int sqLen;
  protected int stLen;


  protected DNAFourierCovnolutionFunction(int sqLen, int stLen, DNA_DataProvider provider) {
    super(provider);
    this.sqLen = sqLen;
    this.stLen = stLen;

  }

  @Override
  public double distance(StringVector_externalID o1, StringVector_externalID o2) {

    char[] o1data = provider.getData(o1.getFileName(), false, o1.getParams());
    char[] o2data = provider.getData(o2.getFileName(), false, o2.getParams());
    
    int o1length = o1data.length;
    int o2length = o2data.length;
    int nSteps = o2length / stLen;
    double maxAlignsSum = 0;

    int fftLength = 1 << (int) (Math.log(o1length + sqLen - 1) / Math.log(2) + 1);
    FFTbase fft = new FFTbase(fftLength, false);
    FFTbase i_fft = new FFTbase(fftLength, true);
       
    float o1R[] = new float[fftLength];
    float o1I[] = new float[fftLength];

    // handle first seq
    for(int i = 0; i < o1length; i++) {
      char curB = o1data[i];
      if(curB == 0) { // 'A' = 1
        o1R[i] = 1;
      }
      else if(curB == 1) { // 'C' = -1
        o1R[i] = 1;
      }
      else if(curB == 2) { // 'G' = i
        o1I[i] = 1;
      }
      else { // 'T' = -i
        o1I[i] = -1;
      }
    }
    fft.fft(o1R, o1I);

    float o2R[] = new float[fftLength];
    float o2I[] = new float[fftLength];
    int o2Start = 0;

    for(int s = 0; s < nSteps; s++) {
      
      // clear o2 arrays
      for(int i = 0; i < fftLength; i++) {
        o2R[i] = 0;
        o2I[i] = 0;
      }

      // create seqence
      // //o2 is reversed + G and T swapped + A and C equal

      for(int i = 0; i < sqLen; i++) {
        char curB = o2data[o2Start+i];
        if(curB == 0) { // 'A' = 1
          o2R[sqLen - i - 1] = 1;
        }
        else if(curB == 1) { // 'C' = -1
          o2R[sqLen - i - 1] = 1;
        }
        else if(curB == 2) { // 'G' = -i //here G and T must be swapped for
                             // i*-i to be 1 == match
          o2I[sqLen - i - 1] = -1;
        }
        else { // 'T' = i
          o2I[sqLen - i - 1] = 1;
        }
      }

      // compute FFT
      fft.fft(o2R, o2I);    
    
      // compute o1FT x o2FT
     
      float[] convR = new float[fftLength];
      float[] convI = new float[fftLength];
      for(int i = 0; i < o1I.length; i++) {
        // ac - bd
        convR[i] = o1R[i] * o2R[i] - o1I[i] * o2I[i];
        // ad + bc
        convI[i] = o1R[i] * o2I[i] + o1I[i] * o2R[i];
      }

      // compute inverse FFT
      
      i_fft.fft(convR, convI);

      // find max and add to average
      float max = Float.NEGATIVE_INFINITY;
      for(float f : convR) {
        if(f > max) {
          max = f;
        }
      }
      maxAlignsSum += sqLen - max;
      
      o2Start += stLen;
    }

    // average best local align score (normalized)
    return maxAlignsSum / (double) nSteps;
  }

  public static class Parametrizer extends AbstractDNAFunction.Parameterizer {

    protected static final OptionID SEQ_LENGTH = new OptionID("dist.seqLength", "Aligning sequence length");
    protected static final OptionID STEP_LENGTH = new OptionID("dist.stepLength", "Aligning step length");


    protected int sqLen = 1;
    protected int stLen = 1;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      IntParameter seqL = new IntParameter(SEQ_LENGTH);
      seqL.addConstraint(new GreaterEqualConstraint(1));
      if(config.grab(seqL)) {
        sqLen = seqL.getValue();
      }
      IntParameter stepL = new IntParameter(STEP_LENGTH);
      stepL.addConstraint(new GreaterEqualConstraint(1));
      if(config.grab(stepL)) {
        stLen = stepL.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNAFourierCovnolutionFunction(sqLen, stLen, provider);
    }

  }

}
