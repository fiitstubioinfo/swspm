package de.lmu.ifi.dbs.elki.datasource.filter.stuba;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.FloatVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalMultipleID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.BSI_DNA_Numerifier;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_Numerifier;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.math.stuba.transform.FFTTransformer;
import de.lmu.ifi.dbs.elki.math.stuba.transform.SpectralTransformer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * 
 * 
 * @author Tomas Farkas
 *
 */
public class DNA_ST_WindowFilter extends AbstractDNAFilter {

  private static Logging LOG = Logging.getLogger(DNA_ST_WindowFilter.class);
  
  private int windowSize;

  private int windowShift;

  private DNA_Numerifier numerifier;

  private SpectralTransformer transform;

  protected DNA_ST_WindowFilter(DNA_DataProvider provider, DNA_Numerifier numerifier, SpectralTransformer transform, int windowSize, int windowShift) {
    super(provider);
    this.numerifier = numerifier;
    this.transform = transform;
    this.windowSize = windowSize;
    this.windowShift = windowShift;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));
    
    if(currentItem instanceof StringVector_externalMultipleID){
      data[inx] = processTask((StringVector_externalMultipleID)currentItem);
    }else{
      data[inx] = processTask(currentItem);
    }    
  } 
  
  protected FloatVector processTask(StringVector_externalID currentItem) {
    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, false, currentItem.getParams());
    float[] result = new float[windowSize];

    if(dataString != null) {
      
      float padFactor = 1;
      if(dataString.length<windowSize){
        int origLength = dataString.length;
        dataString = Arrays.copyOfRange(dataString, 0, windowSize);
        Arrays.fill(dataString, origLength, dataString.length, (char)255);
        
        padFactor = (float)dataString.length / (float)origLength;
      }   
      
      int nWindows = 0;
      // get FFT by windows
      for(int i = 0; i <= dataString.length - windowSize; i += windowShift) {
        char[] actualIndicators = String.copyValueOf(dataString, i, windowSize).toCharArray();
        float[] actualRes = getSpectra(actualIndicators);
        for(int j = 0; j < windowSize; j++) {
          result[j] += actualRes[j];
        }
        nWindows++;
      }
      // normalize result
      for(int i = 0; i < result.length; i++) {
        result[i] = result[i] / nWindows * padFactor;
      }
    }else{
      LOG.warning("could not find data for identifier: "+Arrays.toString(currentItem.getParams()));

    }
    return new FloatVector(result);
  }
  
  @SuppressWarnings("rawtypes")
  protected MultipleTypeVectorList processTask(StringVector_externalMultipleID currentItem) {
   
    int fNum = currentItem.getNumberOfFiles();
    List<FeatureVector> result = new ArrayList<>();
    
    for(int f=0;f<fNum;f++){
     
     String fname = currentItem.getFileName(f);
     char[] dataString = provider.getData(fname, false, currentItem.getParams(f));
     float[] subResult = new float[windowSize];

     if(dataString != null) {
       
       float padFactor = 1;
       if(dataString.length<windowSize){
         int origLength = dataString.length;
         dataString = Arrays.copyOfRange(dataString, 0, windowSize);
         Arrays.fill(dataString, origLength, dataString.length, (char)255);
         
         padFactor = (float)dataString.length / (float)origLength;
       }   
       
       int nWindows = 0;
       // get FFT by windows
       for(int i = 0; i <= dataString.length - windowSize; i += windowShift) {
        
         char[] actualIndicators = String.copyValueOf(dataString, i, windowSize).toCharArray();
         float[] actualRes = getSpectra(actualIndicators);
         for(int j = 0; j < windowSize; j++) {
           subResult[j] += actualRes[j];
         }
         nWindows++;
       }
       // normalize result
       for(int i = 0; i < subResult.length; i++) {
         subResult[i] = subResult[i] / nWindows * padFactor;
       }
       
       result.add(new FloatVector(subResult));
     }else{
       LOG.warning("could not find data for identifier: "+Arrays.toString(currentItem.getParams(f)));
     }
   }
   return new MultipleTypeVectorList(result);
   
  }

  private float[] getSpectra(char[] data) {

    int length = data.length;

    float[][] numericSeqs = numerifier.numerify(data, false);
    float[] spectrum = transform.getSpectrum(true, true, numericSeqs);

    for(int i = 0; i < length; i++) {
      spectrum[i] = spectrum[i] * spectrum[i];
    }
    return spectrum;
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    if(data.getClass().getComponentType().equals(FloatVector.class)){
      res.appendColumn(TypeUtil.FLOAT_VECTOR_FIELD, Arrays.asList(data));
    }else{
      res.appendColumn(TypeUtil.MULTIPLE_TYPE_VECTOR, Arrays.asList(data));
    }
    return res;
  }

  @Override
  protected void createArray_hook() {
    data = new FloatVector[getNrOfTasks()];
    
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID NUMERIFIER = new OptionID("filter.numerifier", "Type of DNA numerification method");

    protected static final OptionID TRANSFORM = new OptionID("filter.transform", "Type of spectral transform");

    protected static final OptionID WINW_SIZE = new OptionID("filter.windowSize", "Size of sliding window, power of 2");

    protected static final OptionID WINW_SHIFT = new OptionID("filter.windowShift", "Window shift in 1 step");

    private DNA_Numerifier numerifier;

    private SpectralTransformer transform;

    private int windowSize;

    private int windowShift;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      ObjectParameter<DNA_Numerifier> nuP = new ObjectParameter<>(NUMERIFIER, DNA_Numerifier.class, BSI_DNA_Numerifier.class);
      ObjectParameter<SpectralTransformer> trP = new ObjectParameter<>(TRANSFORM, SpectralTransformer.class, FFTTransformer.class);

      IntParameter winSP = new IntParameter(WINW_SIZE);
      winSP.addConstraint(new GreaterConstraint(0));

      IntParameter winSH = new IntParameter(WINW_SHIFT);
      winSH.addConstraint(new GreaterConstraint(0));

      if(config.grab(nuP)) {
        numerifier = nuP.instantiateClass(config);
      }
      if(config.grab(trP)) {
        transform = trP.instantiateClass(config);
      }
      if(config.grab(winSP)) {
        windowSize = winSP.getValue();
      }
      if(config.grab(winSH)) {
        windowShift = winSH.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNA_ST_WindowFilter(provider, numerifier, transform, windowSize, windowShift);
    }

  }

}
