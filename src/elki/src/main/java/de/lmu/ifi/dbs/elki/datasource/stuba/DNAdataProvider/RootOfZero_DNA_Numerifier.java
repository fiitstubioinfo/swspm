package de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.DNA_WordFrequencyFilter;
import de.lmu.ifi.dbs.elki.distance.distancefunction.stuba.SparseVectorComparator.SparseCompareType;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.LessEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.EnumParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class RootOfZero_DNA_Numerifier implements DNA_Numerifier {

  private boolean complementTogether;

  public RootOfZero_DNA_Numerifier(boolean complementaryCoding) {
    this.complementTogether = complementaryCoding;
  }

  @Override
  public float[][] numerify(char[] inSeq, boolean dense) {

    int length = inSeq.length;
    if(dense) {
      length *= 4;
    }
    float[] re = new float[length];
    float[] im = new float[length];

    if(complementTogether){
    if(!dense) {
      for(int i = 0; i < inSeq.length; i++) {
        char curB = inSeq[i];
        switch(curB){
        case 0: re[i] = 1; break;
        case 1: im[i] = -1; break;
        case 2: im[i] = 1; break;
        case 3: re[i] = -1;break;
        }
      }
    }
    else {
      for(int i = 0; i < inSeq.length; i++) {
        char curB = inSeq[i];
        for(int j = 0; j < 4; j++) {

          char curb = (char) (curB & 3);
          curB = (char) (curB >> 2);

          switch(curb){
          case 0: re[i * 4 + j] = 1; break;
          case 1: im[i * 4 + j] = -1; break;
          case 2: im[i * 4 + j] = 1; break;
          case 3: re[i * 4 + j] = -1; break;
          }
        }
      }
    }
    }else{
      if(!dense) {
        for(int i = 0; i < inSeq.length; i++) {
          char curB = inSeq[i];
          switch(curB){
          case 0: re[i] = 1; break;
          case 1: re[i] = -1; break;
          case 2: im[i] = 1; break;
          case 3: im[i] = -1;break;
          }
        }
      }
      else {
        for(int i = 0; i < inSeq.length; i++) {
          char curB = inSeq[i];
          for(int j = 0; j < 4; j++) {

            char curb = (char) (curB & 3);
            curB = (char) (curB >> 2);

            switch(curb){
            case 0: re[i * 4 + j] = 1; break;
            case 1: re[i * 4 + j] = -1; break;
            case 2: im[i * 4 + j] = 1; break;
            case 3: im[i * 4 + j] = -1; break;
            }
          }
        }
      }  
      
    }
    return new float[][] { re, im };
  }

  public enum EncodingType {
    COMPLEMENTS_TOGETHER, COMPLEMENTS_CROSS
  }
  
  public static class Parameterizer extends AbstractParameterizer {

    protected static final OptionID ENCODING = new OptionID("numerifier.encoding", "ACGT = {1,-1,i,-i} or {1,-i,i,-1}");

    protected EncodingType encoding;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      final EnumParameter<EncodingType> param = new EnumParameter<>(ENCODING, EncodingType.class, EncodingType.COMPLEMENTS_TOGETHER);
      if(config.grab(param)) {
        encoding = param.getValue();
      }      
    }

    @Override
    protected RootOfZero_DNA_Numerifier makeInstance() {
      return new RootOfZero_DNA_Numerifier(encoding==EncodingType.COMPLEMENTS_TOGETHER);
    }
  }

}
