package de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical;

import java.util.Map;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2015
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.database.datastore.DBIDDataStore;
import de.lmu.ifi.dbs.elki.database.datastore.DoubleDataStore;
import de.lmu.ifi.dbs.elki.database.ids.DBIDs;
import de.lmu.ifi.dbs.elki.database.relation.Relation;

/**
 * DEPRECATED - USE ExportLabelledVisualization INSTEAD
 * 
 * 
 * The pointer representation of a hierarchical clustering. Each object is
 * represented by a parent object and the distance at which it joins the parent
 * objects cluster. This is a rather compact and bottom-up representation of
 * clusters, the class
 * {@link de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.extraction.ExtractFlatClusteringFromHierarchy}
 * can be used to extract partitions from this graph.
 *
 * This class can also compute dendrogram positions, but using a faster
 * algorithm than the one proposed by Sibson 1971, using only O(n log n) time
 * due to sorting, but using an additional temporary array.
 *
 * @author Tomas Farkas
 * @param <O>
 * @since 0.6.0
 * 
 * 
 */
public class LabelledPointerHierarchyRepresentationResult extends PointerHierarchyRepresentationResult {
  
  /**
   * Captions for entries in result
   */
  Map<Integer, String> captions = null;  
  Relation<?> relation = null;
  
  @Deprecated
  public LabelledPointerHierarchyRepresentationResult(DBIDs ids,  Map<Integer, String> captions, DBIDDataStore parent, DoubleDataStore parentDistance) {
    super(ids, parent, parentDistance);
    this.captions = captions;    
  }  
  
  public LabelledPointerHierarchyRepresentationResult(DBIDs ids, Relation<?> r, DBIDDataStore parent, DoubleDataStore parentDistance) {
    super(ids, parent, parentDistance);
       this.relation = r;    
  } 
  
  public String getCaption(int id){
    return captions == null ? null : captions.get(id);
  }
  
  public Relation<?> getRelation(){
    return relation;
  }
  
}
